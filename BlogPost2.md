Simple Use of Java 8 Lambda Functions for Writing CSV Files
---


Constraints:

    - dont modify entitys (with annotations)
    - dont explicitely use reflection
    
Method 1: manual way

    - using opencsv to write raw data string array, manually create Person pojos
    - can have an enum with PERSON_FIELDS, with each fieldname stated
    
Method 2: lambda way

    - using opencsv to write raw data string array, however use PERSON_FIELDS enum with lambda functins to getters to write create array
    

Method 3: Jackson CSV format way

    - using jackson CSV, create a new implementation of Person called PersonBean, which has the proper annotations.
    
Alternative:
    
    - mention that jackson has that special mapping infra
    
    
Other things:

 - using kaughagah maps to compress base sequences in a performant way
    
    
    

