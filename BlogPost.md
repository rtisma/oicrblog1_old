Simple Use of Java 8 Lambda Functions for Writing TSV Files
---



#Introduction
With the introduction of Java 8 Lambda Functions, a plethora of new design tech
o


In the persuit of simple, efficient and performant code for writing CSV files, I have  

When writing CSV files, one of the biggest issues is mapping field data to field names with a specific order.  

One of the biggest issues when writing CSV files, is mapping entity field data to csv field names and in the correct order. 

One important feature in processing CSV files is mapping 

Many CSV processing frameworks/libraries that exist today depend heavily on runtime annotation processing (via reflection) to map Java object field data to CSV basicFields. In this blog post, I will explore a simple yet effective solution using Java 8 lambda functions, to perform this mapping without the use of annotation processing, while still being able to piggyback the processing capabilities of many CSV processing libraries.


# Background
Surely, every developer at one point in their life has "rolled their own" csv parser and writer, and only after hitting the many edge cases, abandoned their solution to use a more refined, tested and performant CSV parsing/writing libary. In the Java world, a plethora of open-source CSV parsers and writers exist, such as [OpenCSV](http://opencsv.sourceforge.net/), [SuperCSV](http://supercsv.sourceforge.net/), [Apache Commons CSV](http://commons.apache.org/proper/commons-csv), [jackson-dataformat-csv](http://github.com/FasterXML/jackson-dataformat-csv) and the list goes on. Each one of these libraries share a very important and common feature: to read and write files while handling all the typical edge cases involved with [RFC-4180](https://www.rfc-editor.org/rfc/rfc4180.txt) compliant CSV text.


Since all these CSV processing libraries share the same common features (obviously with different performance characteristics), what else differentiates them? One important feature is mapping a [POJO](https://en.wikipedia.org/wiki/Plain_old_Java_object) (which is a mutable Java class that contains an empty, argument-less constructor, with a getter and setter method for each field) to a CSV field name, which can alternatively be described as configuring an data object using a schema. All libraries essentially convert a line of CSV text to a `String[]` under the hood, however the magic lies in configuring a POJO using a CSV schema. 

CSV schemas effectively map getters and setters to a field name. One of the ways this is implemented is through annotations which bind POJO member basicFields to a schema. The CSV library will create a schema by binding the field name with the same name as the POJO member name, with its associated setter and getter methods. The benefit to this is that the CSV schema is automatically generated, which means changing field names is as easy as changing the name of the POJO member<sup>[1](#footnote1)</sup>. As an example, suppose the CSV data looks like:

| id | firstName | lastName   | age |
|----|-----------|------------|-----|
| 1  | Denis     | Ritchie    | 70  |
| 2  | Frank     | Rosenblatt | 43  |
| 3  | Alex      | Murphy     | 37  |

and if we are using OpenCSV, the pojo, called `Person` would be annotated using the `@CsvBindByName` annotation which binds the data field with a column name in the CSV input.

```java
public class Person {

    @CsvBindByName 
    private Integer id;
    
    @CsvBindByName 
    private String firstName;
    
    @CsvBindByName 
    private String lastName;
    
    @CsvBindByName 
    private Integer age;
    
    public Person(){}
    
    // Getters and Setters
}
```

Using OpenCSV is as simple as the code below, which will allow you to convert a CSV file to a `List<Person>` and convert a `List<Person>` to a CSV file.
```java
public class CSVProcessor {
  
  public static <T> List<T> read(Path csvFilePath, char separator, Class<T> clazz) throws java.io.Exception{
    CSVReader csvReader = new CSVReader(new FileReader(csvFilePath.toFile()), separator);
    HeaderColumnNameMappingStrategy<clazz> strategy = new HeaderColumnNameMappingStrategy<>();
    strategy.setType(clazz);
    CsvToBean<clazz> csvToBean = new CsvToBean<>();
    return csvToBean.parse(strategy,csvReader);
  }
  
  public static <T> void write(Path csvFilePath, char separator, List<T> objects, Class<T> clazz) throws java.io.Exception{
    Writer writer = new FileWriter(csvFilePath.toFile());
    StatefulBeanToCsv<T> beanToCsv = new StatefulBeanToCsvBuilder<T>(writer)
                                            .withMappingStrategy()
                                            .withSeparator(separator).build();
    beanToCsv.write(objects);
  }
  
}
```

The annotation method is great, however using it on legacy code can be an issue if the code is not modifiable (say in a jar or code-freeze). Another method is to manually define your own CSV schema by mapping the correct setter and getter functions to each field name using Java 8 Method references. The advantage of this approach is obvious: you dont have to modify the existing with annotations to create a CSV schema. This approach is very simple, compile time based, and preserves order. Continueing with the same CSV data as the annotation example, the pojo woul


and the pojo, called `Person`, looks like,

```java
public class Person {

    private String id;
    private String firstName;
    private String lastName;
    private String age;
    
    public Person(){}
    
    // Getters and Setters
}
```

and to create your manual schema, you would have the following implementation:


```java
public class TestClass {
  private static final PersonConverters PERSON_CONVERTERS = new PersonConverters();
  
  // This function mimics what the CSV parsers would output
  public static List<String[]> generateData(){
    List<String[]> list = new ArrayList<String[]>();
    list.add(new String[]{"1", "Denis", "Ritchie", "70"});
    list.add(new String[]{"2", "Frank", "Rosenblatt", "43"});
    list.add(new String[]{"3", "Alex", "Murphy", "37"});
    return list;
  }
  
  public static void main(String[] args){
    for (String[] inputData: generateData()){
      // Convert input data array to pojo
      Person person  = PERSON_CONVERTERS.convertToPerson(inputData);
      
      System.out.prinln("Person.id: "+person.getId());
      System.out.prinln("Person.firstName: "+person.getFirstName());
      System.out.prinln("Person.lastName: "+person.getLastName());
      System.out.prinln("Person.age: "+person.getAge());
      
      // Convert pojo back to data array
      String[] outputData = PERSON_CONVERTERS.convertToArray(person);
      
      // Assert that input and output data array are equal
      for (int i=0; i< inputData.length; i++){
        assert(inputData[i].equals(outputData[i]));
      }
    }
  }
  
  /**
  * Converts String[] data to a Person object, and viseversa
*/
  public static class PersonConverters {
    
    public Person convertToPerson(String[]  data){
        Person person = new Person();
        person.setId(data[0]);
        person.setFirstName(data[1]);
        person.setLastName(data[2]);
        person.setAge(data[3]);
        return person;
    }
    
    public String[] convertToArray(Person person){
        return new String[]{
            person.getId(), 
            person.getFirstName(), 
            person.getLastName(), 
            person.getAge()};
    }
  }
}
```



which can alternatively be described as _mapping behavior to a field name_. 

Libraries such as OpenCSV





```java
public class Person {

    @CsvBindByName 
    private Integer id;
    
    @CsvBindByName 
    private String firstName;
    
    @CsvBindByName 
    private String lastName;
    
    @CsvBindByName 
    private Integer age;
    
    public Person(){}
    
    // Getters and Setters
}
```

```java
public enum PersonField {
  ID("id", Person::getId, Person::setId),
  FIRSTNAME("firstName", Person::getFirstName, Person::setFirstName),
  LASTNAME("lastName", Person::getLastName, Person::setLastName),
  AGE("age", Person::getAge, Person::setAge);
  
  private PersonField(String fieldname, Function<Person, Object> getter, <Person, ? extends Object>)
  
}

```

Most libraries support parsing a line and output a `String[]`, leaving the user to manage mapping column numbers to basicFields manually. 
This can be achieved by hardcoded mapping, configuration-file based mapping (i.e properties file or xml file), or through java annotations. Hardcoded mapping 


Although annotations can simplify CSV processing, they create a dependancy for the annotated source code to a CSV library. For code that is expected to change often or for experimental CSV dumps, adding annotations is not neccessarily the cleanest option. For instance, say you have POJO or model from an external library, that is used for some existing service, and all you want to do is write it to a CSV file. If the object implements an interface, you could locally decorate the interface with the object, and on the decoration add the correct CSV annotation (essentially wrapping the object). 



In this post, I will explore an interesting alternative to mapping behavior to a field name using Java 8 Lambda functions. With the introduction to lambda functions in Jav


all the libraries essentially convert  a CSV line of text, to a String array when parsing, and converting a string array to a csv line of text when writing, while adhering to the rules defined in [RFC-4180](https://www.rfc-editor.org/rfc/rfc4180.txt). Therefore, CSV parsers can be use IN CONJUNCTION with the lambda mapping method: you the get the best of both worlds, no annotation processing

# footnote1
Aside from using the POJO member name as the CSV fieldname, some CSV binding annotations have parameters for specifing **custom** field names.

#Discarded

Since annotation processing uses the java reflection api to introspect annotated code, a runtime analysis is required. The general rule of thumb with efficient software development is catching software bugs or unwanted behavior as early on in the process as possible. In the context of Java (or any compiled language), this means catching bugs or errors at compilation is much better than catching them at runtime. As an example, you can annotate a pojo with `@CSVBindByName` (using the OpenCSV annotation library) 


pros/cons of annotation processing vs lambda way. When would you not want to use annotation processing

