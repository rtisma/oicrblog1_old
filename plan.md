Simple Use of Java 8 Lambda Functions for Writing TSV Files
---



#Introduction
-  what problem is:
    - dealing with 500+ million lines...need something very very performant
    - input data is not a csv file, so didnt need reader
- purpose of this blog post: to show simple solution not dependant on large frameworks. kind of like a case study
    
# Background
- specifications:
    - fast at runtime. No annotation processing or reflection 
    - everything must be defined at compile time,(i.e dont rely on depManagment framework to wire configuration together or Factories to build configurations)
    - minimize the amount of moving parts (ie files), which mainting high cohesion and fairly loosely coupled
    
    
- Alternative1(Naive Way):
    - most naive way, just create a `getHeader()` and `getOutputTsvLine()` methods with explicit definitions of the field names and their values
    - Pros:
        - very simple
    - cons:
        - very error prone, since you must match header name order with outputtsv order
        - makes future changes tricky and error prone
        - if you change the `getHeader()` method, you must also change the `getOutputTsvLine()` method
        
- Alternative2(Annotated Pojo):
    - Fields in a Pojo are annotated with annotations from a particular TSV library(opencsv, supercsv,jackson-dataformat-csv, univocity csv parser...etc ), the annotations are also order sensitive
    - Pros:
        - can specify TSV field name explicitly in the annotation, or implicitly by using the Pojo member field name
        - very fast setup if using dependancy manager such as Maven, Gradle, ..etc
        - uses Reflection to create mapping of field names and associated getter methods
    - Cons:
        - requires use of external library, and therefore Dependancy manager (e.g Apache Maven), or manual classpath manipulation (i.e using GNU Makefiles)
        - can be very slow for large TSV files, depending on how annotation processing is implemented
        - require java 5 (not really an issue)
        
- Alternative3(lambda way):
    - allows you to map a name to more than one behavior (such as Pojo getters, or some other function), AT COMPILE TIME (i.e if getter doesnt exist, will fail at compile time, as opposed to annotation method, which will fail at runtime)
    - create an Enum with each Enumeration having 2 members: `String name` and `Function<MyPojo, String> functor`
    - create compile-time mapping of the fieldname with the getter function to be called
    - explicit mapping all in one file, nicely encapsulated outside business logic of pojo
    - can allow for more _functors_ to be mapped to the field name
    - Pros:
        - compile time mapping of fieldname to behavior
        - more than one behavior can be mapped to a field name
        - explicit mapping and ordering while encapsulated outside of Pojo or Entity
    - Cons:
        - requires Java 8
        - not useful for when reading 

- show diagram of problem

- just calling Method.invoke and Function.apply, for 1Billion calls, Function.apply was 38% faster, InvokeMethodology:  9,732,455 calls/sec, FunctionMethodology:  13,408,780 calls/sec


#Questions:
1. can  annotation processing being runtime or compile time?
2. read up on general process of [writing your own annotation processor](https://www.javacodegeeks.com/2015/09/java-annotation-processors.html), [how annotations work java](https://dzone.com/articles/how-annotations-work-java)
3. read why [java annotations are a big mistake](https://dzone.com/articles/java-annotations-are-a-big-mistake?fromrel=true)

Reference:
[Comparison of CSV Parser Performance](https://github.com/uniVocity/csv-parsers-comparison)
[Annotation Processing](https://www.objc.io/issues/11-android/dependency-injection-in-java/)
